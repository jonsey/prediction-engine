import lstm
import data_manager
import utils.time_utils
import time
import sys
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


def plot_results_multiple(predicted_data, true_data, prediction_len, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    #Pad the list of predictions to shift it in the graph to it's correct start
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(i * prediction_len)]
        plt.plot(padding + data, label='Prediction')
        # plt.legend()
    # plt.show()
    fig.savefig('/projects/super-trader-ai/data/training/' + predictions_file_name + '.png')


#Main Run Thread
if __name__=='__main__':
    global_start_time = time.time()
    epochs  = 1
    seq_len = 50
    prediction_len = 50
    file_name = sys.argv[1:][0]
    start_time = utils.time_utils.timestamp_days_ago(30)

    print('> Loading data... ')

    X_train, y_train, X_test, y_test, raw_data = data_manager.handle_training_data(file_name, start_time, seq_len, True)
    print(y_train)

    print('> Data Loaded. Compiling...')

    model = lstm.build_model([1, 50, 100, 1])

    model.fit(
        X_train,
        y_train,
        batch_size=50,
        nb_epoch=epochs,
        verbose=1,
        validation_split=0.05)

    lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/', file_name)

    predictions = lstm.predict_sequences_multiple(model, X_test, seq_len, prediction_len)

    print('Training duration (s) : ', time.time() - global_start_time)
    plot_results_multiple(predictions, y_test, prediction_len, file_name)
