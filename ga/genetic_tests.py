import unittest
from types import *
import genetic
from chromosome import Gene

class TestInitialPopulation(unittest.TestCase):
    def setUp(self):

        self.size_of_pop = 2
        params =  {
        'seq_len': {'val': 25, 'min': 0, 'max': 100, 'type': IntType},
             'i1': {'val': 64, 'min': 2, 'max': 512, 'type': IntType}
        }
        self.p = genetic.population(self.size_of_pop, len(params), params)

    def test_initial_pop_size(self):
        self.assertEqual(self.size_of_pop, len(self.p))

    def test_initial_population(self):
        name_of_first_gene = self.p[0].genes[0].name
        self.assertTrue(len(self.p) == 2)
        self.assertEqual(name_of_first_gene, 'i1')

    def test_initial_pop_individual_value_range(self):
        value = self.p[0].genes[0].val
        self.assertEqual('i1', self.p[0].genes[0].name)
        self.assertTrue(value >= 2)
        self.assertTrue(value <= 512)

    def test_initial_pop_individual_value_range_variable(self):
        value = self.p[0].genes[1].val
        self.assertEqual('seq_len', self.p[0].genes[1].name)
        self.assertTrue(value >= 0)
        self.assertTrue(value <= 100)

    def test_get_gene_value_from_chromosome(self):
        chromosome = self.p[0]
        self.assertIsInstance(chromosome.gene('seq_len'), Gene)


class TestFitness(unittest.TestCase):

    def setUp(self):
        self.cost_function = lambda individual: 8
        size_of_pop = 10
        target = 10 # percent profit
        params =  {
        'seq_len': {'val': 25, 'min': 0, 'max': 100, 'type': IntType},
             'i1': {'val': 64, 'min': 2, 'max': 512, 'type': IntType}
        }
        p = genetic.population(size_of_pop, len(params), params)
        self.fitness_history = [genetic.grade(p, target, self.cost_function),]
        for i in xrange(100):
            p = genetic.evolve(self.cost_function, p, target)
            self.fitness_history.append(genetic.grade(p, target, self.cost_function))

    def test_initial_fitness(self):
        # print(self.fitness_history)
        pass


if __name__ == '__main__':
    unittest.main()
