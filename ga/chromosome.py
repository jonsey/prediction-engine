from random import randint, random

class Chromosome:

    def __init__(self, genes):
        self.genes = genes

    def __str__(self):
        [ str(x) for x in self.genes]

    def gene(self, key):
        return next((item.val for item in self.genes if item.name == key), None)

    def mutate(self):
        pos_to_mutate = randint(0, len(self.genes)-1)
        self.genes[pos_to_mutate].mutate()

    @staticmethod
    def crossover(male, female):
        splice_index = randint(1, len(male.genes) - 2)
        new_genes = male.genes[:splice_index] + female.genes[splice_index:]
        return Chromosome(new_genes)

class Gene:
    def __init__(self, name, val, min, max):
        self.name = name
        self.val = val
        self.min = min
        self.max = max

    def __str__(self):
        print('{}: {}'.format(self.name, self.val))

    def create_random_mutation():
        val = randint(self.min, self.max)
        return Gene(self.name, val, self.min, self.max)

    def mutate(self):
        self.val = randint(self.min, self.max)
