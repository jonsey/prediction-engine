"""
# Example usage
from genetic import *
target = 371
p_count = 100
i_length = 6
i_min = 0
i_max = 100
p = population(p_count, i_length, i_min, i_max)
fitness_history = [grade(p, target),]
for i in xrange(100):
    p = evolve(p, target)
    fitness_history.append(grade(p, target))

for datum in fitness_history:
   print datum
"""
from random import randint, random
from operator import add
from chromosome import Chromosome, Gene

def individual(x, params):
    'Create a member of the population.'

    genes = []
    for name in params.keys():
        values = params[name]
        gene = Gene(name, values['val'], values['min'], values['max'])
        if x > 0 :
            gene.mutate()
        genes.append(gene)

    return Chromosome(genes)

def population(count, length, params):
    """
    Create a number of individuals (i.e. a population).

    count: the number of individuals in the population
    length: the number of values per individual
    params: initial chromosome

    """
    return [individual(x, params) for x in xrange(count)]

def print_chromosome(chromosome):
    for gene in chromosome.genes:
        print('{}: {}'.format(gene.name, gene.val))

def fitness(individual, target, cost_function, currency):
    """
    Determine the fitness of an individual. Higher is better.

    individual: the individual to evaluate
    target: the target number individuals are aiming for
    """
    print_chromosome(individual)
    percent_correct_buy, profit_loss_ratio = cost_function(currency, individual)

    # fitness = min(percent_correct_buy, profit_loss_ratio)
    fitness = profit_loss_ratio
    print('Fitness: {}'.format(fitness))
    print('=============================================================================')
    return 100 - fitness

def grade(pop, target, cost_function, currency):
    'Find average fitness for a population.'
    summed = reduce(add, (fitness(x, target, cost_function, currency) for x in pop))
    return summed / (len(pop) * 1.0)

def evolve(currency, cost_function, pop, target, retain=0.2, random_select=0.05, mutate=0.01):
    graded = [ (fitness(x, target, cost_function, currency), x) for x in pop]
    graded = [ x[1] for x in sorted(graded)]
    print(graded)
    retain_length = int(len(graded)*retain)
    parents = graded[:retain_length]
    # randomly add other individuals to
    # promote genetic diversity
    for individual in graded[retain_length:]:
        if random_select > random():
            parents.append(individual)
    # mutate some individuals
    for individual in parents:
        if mutate > random():
            individual.mutate()
    # crossover parents to create children
    parents_length = len(parents)
    desired_length = len(pop) - parents_length
    children = []
    while len(children) < desired_length:
        male_index = randint(0, parents_length-1)
        female_index = randint(0, parents_length-1)
        if male_index != female_index:
            male = parents[male_index]
            female = parents[female_index]
            child = Chromosome.crossover(male, female)
            children.append(child)
    parents.extend(children)
    return parents
