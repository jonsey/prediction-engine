def label(series, trade_period, stop_percent, limit_percent):
    for index, close in enumerate(series["close"][:-trade_period]):
        _hit_limit_without_hiting_stop = _price_increased_by(trade_period, series, close, index, stop_percent, limit_percent)
        _hit_stop_without_hiting_limit = _price_decreased_by(trade_period, series, close, index, stop_percent, limit_percent)

        if _hit_limit_without_hiting_stop:
            series["target"][index] = 1
        elif _hit_stop_without_hiting_limit:
            series["target"][index] = 0
        else:
            series["target"][index] = 0

    return series

def _price_increased_by(trade_period, series, starting_close, current_index, stop_percent, limit_percent):
    _required_rise = starting_close * (1.0 + (stop_percent/100.0))
    _required_stop_loss = starting_close - (starting_close * (limit_percent/100.0))

    for x in range(1, trade_period + 1):
        if(series["close"][x + current_index] >= _required_rise):
            return True
        if(series["close"][x + current_index] <= _required_stop_loss):
            return False

    return False

def _price_decreased_by(trade_period, series, starting_close, current_index, stop_percent, limit_percent):
    _required_rise = starting_close * (1.0 + (stop_percent/100.0))
    _required_stop_loss = starting_close - (starting_close * (limit_percent/100.0))

    for x in range(1, trade_period + 1):
        if(series["close"][x + current_index] <= _required_stop_loss):
            return True
        if(series["close"][x + current_index] >= _required_rise):
            return False

    return False
