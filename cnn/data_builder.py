import series_categoriser
import frequency_sampler

class Loader(object):

    def get_data_(self, data_length_hours, period):
        start_time = utils.time_utils.timestamp_hours_ago(data_length_hours)
        return requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=' + self.currency_pair + '&start='+ str(start_time) +'&end=9999999999&period=' + str(period))


    def __init__(self, currency_pair, trade_period, stop_percent, limit_percent):
        self.currency_pair = currency_pair

        df = read_json(response.text, orient='records')
        df = df.set_index(['date'])
        df = df.drop('weightedAverage', axis=1)
        df = df.drop('open', axis=1)
        df = df.drop('volume', axis=1)

        series = ta.create_ma_series(series, 4, "sma_4")
        series = ta.create_ma_series(series, 9, "sma_9")
        series = ta.create_ma_series(series, 12, "sma_12")
        series = ta.create_ma_series(series, 24, "sma_24")

        df["target"] = Series()
        series = series_categoriser.label(series, trade_period, stop_percent, limit_percent)

        downsampledSeries1 = frequency_sampler.downsample(df, frequency=30)
        downsampledSeries2 = frequency_sampler.downsample(df, frequency=60)
        downsampledSeries3 = frequency_sampler.downsample(df, frequency=240)

        downsampledSeries1 = series_categoriser.label(downsampledSeries1, trade_period, stop_percent, limit_percent)
        downsampledSeries2 = series_categoriser.label(downsampledSeries2, trade_period, stop_percent, limit_percent)
        downsampledSeries3 = series_categoriser.label(downsampledSeries3, trade_period, stop_percent, limit_percent)
