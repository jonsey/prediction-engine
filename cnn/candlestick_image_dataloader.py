from __future__ import division
import series_categoriser
import pandas as pd
import numpy as np
import requests
from pandas import read_json
from pandas import Series
from keras.utils import np_utils
from sklearn import preprocessing

import utils.time_utils

class loader(object):

    BLACK = 1
    GREEN = 2
    RED = 3
    image_height = 500
    candle_width = 1

    def candle_colour(self, open_, close):
        if (close > open_):
            return self.GREEN
        else:
            return self.RED

    def normalised_scaled_value(self, value_range, min_value, orig_value):
        value = (1 - ((value_range - (orig_value - min_value))/value_range)) * self.image_height
        return np.round(value)

    def green_candle_row_colour(self, row, close_row, high_row, low_row, open_row):
        colour = 0
        if(high_row >= row and low_row <= row):
            colour = self.BLACK
        if(close_row >= row and open_row <= row):
            colour = self.GREEN
        return colour

    def red_candle_row_colour(self, row, close_row, high_row, low_row, open_row):
        colour = 0
        if(high_row >= row and low_row <= row):
            colour = self.BLACK
        if(open_row >= row and close_row <= row):
            colour = self.RED
        return colour

    def encode_sequence(self, sequence_df):
        candle_width = self.candle_width
        sequence_len = len(sequence_df)
        width = sequence_len * candle_width
        height = self.image_height

        max_value = sequence_df["high"].max()
        min_value = sequence_df["low"].min()
        value_range = max_value - min_value

        X = np.zeros((height, int(width)))

        for col in range(0, sequence_len):
            close = sequence_df["close"][col]
            high = sequence_df["high"][col]
            low = sequence_df["low"][col]
            open_ = sequence_df["open"][col]

            close_row = self.normalised_scaled_value(value_range, min_value, close)
            high_row = self.normalised_scaled_value(value_range, min_value, high)
            low_row = self.normalised_scaled_value(value_range, min_value, low)
            open_row = self.normalised_scaled_value(value_range, min_value, open_)

            sub_col = col*candle_width
            for row in range(0, height):
                if(self.candle_colour(open_, close) == self.GREEN):
                    X[row, col] = self.green_candle_row_colour(row, close_row, high_row, low_row, open_row)

                else:
                    X[row, col] = self.red_candle_row_colour(row, close_row, high_row, low_row, open_row)

        X = np.flipud(X)
        # np.savetxt('/projects/super-trader-ai/data/df.csv', sequence_df, delimiter=',')
        # np.savetxt('/projects/super-trader-ai/data/encodedX.csv', X, delimiter=',')

        return X.flatten()


    def get_data_(self, data_length_hours, period):
        start_time = utils.time_utils.timestamp_hours_ago(data_length_hours)
        return requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=' + self.currency_pair + '&start='+ str(start_time) +'&end=9999999999&period=' + str(period))

    def __init__(self, currency_pair, seq_len, data_length_hours, period, trade_period, stop_percent, limit_percent):
        self.currency_pair = currency_pair

        response = self.get_data_(data_length_hours, period)
        df = read_json(response.text, orient='records')
        df = df.set_index(['date'])
        df = df.drop('weightedAverage', axis=1)
        df = df.drop('volume', axis=1)
        df = df.drop('quoteVolume', axis=1)

        df["target"] = Series()
        df = series_categoriser.label(df, trade_period, stop_percent, limit_percent)

        X = np.zeros((len(df) - seq_len, seq_len * self.image_height))

        for index in range(len(df) - seq_len):
            sequence_df = df[index:index+seq_len]
            X[index] = self.encode_sequence(sequence_df)

        Y = np_utils.to_categorical(df["target"][:-seq_len], 2)

        test_split = int(len(X) * 0.8)

        self.X_train = X[:test_split]
        self.Y_train = Y[:test_split]

        self.X_test = X[test_split:]
        self.Y_test = Y[test_split:]

        self.X_train = self.X_train.reshape(self.X_train.shape[0], 1, seq_len * self.candle_width, self.image_height ).astype('float32')
        self.X_test = self.X_test.reshape(self.X_test.shape[0], 1, seq_len * self.candle_width, self.image_height ).astype('float32')
