from keras.models import Model # basic class for specifying and training a neural network
from keras.layers import Input, Convolution1D, MaxPooling1D, Dense, Dropout, Flatten
from keras.utils import plot_model


def conv_block(inp, conv_depth_1, kernel_size, pool_size, conv_depth_2, drop_prob_1, drop_prob_2):
    # Conv [32] -> Conv [32] -> Pool (with dropout on the pooling layer)
    conv_1 = Convolution1D(conv_depth_1, kernel_size * 2, padding='same', name="first", activation='relu')(inp)
    conv_2 = Convolution1D(conv_depth_1, kernel_size * 2, padding='same', activation='relu')(conv_1)
    pool_1 = MaxPooling1D(pool_size=pool_size * 2)(conv_2)
    drop_1 = Dropout(drop_prob_1)(pool_1)
    # Conv [64] -> Conv [64] -> Pool (with dropout on the pooling layer)
    conv_3 = Convolution1D(conv_depth_2, kernel_size, padding='same', activation='relu')(drop_1)
    conv_4 = Convolution1D(conv_depth_2, kernel_size, padding='same', activation='relu')(conv_3)
    pool_2 = MaxPooling1D(pool_size=pool_size)(conv_4)
    drop_2 = Dropout(drop_prob_1)(pool_2)

    return drop_2

def build_model(seq_len, depth, conv_depth_1, kernel_size, pool_size, conv_depth_2, drop_prob_1, drop_prob_2, hidden_size, num_classes):
    inp = Input(shape=(seq_len, depth)) # depth goes last in TensorFlow back-end (first in Theano)

    drop_2 = conv_block(inp, conv_depth_1, kernel_size, pool_size, conv_depth_2, drop_prob_1, drop_prob_2)

    # Now flatten to 1D, apply FC -> ReLU (with dropout) -> softmax
    flat = Flatten()(drop_2)
    hidden = Dense(hidden_size, activation='relu')(flat)
    drop_3 = Dropout(drop_prob_2)(hidden)
    out = Dense(num_classes, activation='softmax')(drop_3)

    model = Model(inputs=inp, outputs=out) # To define a model, just specify its input and output layers

    model.compile(loss='categorical_crossentropy', # using the cross-entropy loss function
              optimizer='adam', # using the Adam optimiser
              metrics=['accuracy']) # reporting the accuracy

    return model
