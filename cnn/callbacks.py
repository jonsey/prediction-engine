import numpy as np
from keras.callbacks import Callback
from keras import backend as K
from keras import models

class FilterActivations(Callback):

    def on_epoch_end(self, epoch, logs):
        model = self.model
        activations = []
        layer_outputs = [layer.output for layer in model.layers if
               layer.name == "first" ]  # all layer outputs
