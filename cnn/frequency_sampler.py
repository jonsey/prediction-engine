import datetime
import pandas as pd
import numpy as np

def downsample(df, frequency):
    index = df["date"]
    d = {'date': df["date"], 'close': df["close"], 'high': df["high"], 'low': df["low"]}
    new_df = pd.DataFrame(index=index, data=d)
    return series.resample(frequency + 'M').apply(custom_resampler)

def custom_resampler(array_like):
    return array_like[-1]
