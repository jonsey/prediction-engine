import series_categoriser
import pandas as pd
import numpy as np
import requests
from pandas import read_json
from pandas import Series
from keras.utils import np_utils
from sklearn import preprocessing

import utils.time_utils

class loader(object):

    def get_data_(self, data_length_hours, period):
        start_time = utils.time_utils.timestamp_hours_ago(data_length_hours)
        return requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=' + self.currency_pair + '&start='+ str(start_time) +'&end=9999999999&period=' + str(period))

    def add_row(self,df, index):
        close = df["close"][index]
        high = df["high"][index]
        low = df["low"][index]
        open_ = df["open"][index]
        return np.array([close, high, low, open_])

    def __init__(self, currency_pair, seq_len, data_length_hours, period, trade_period, stop_percent, limit_percent):
        self.currency_pair = currency_pair

        response = self.get_data_(data_length_hours, period)
        df = read_json(response.text, orient='records')
        df = df.set_index(['date'])
        df = df.drop('weightedAverage', axis=1)
        df = df.drop('volume', axis=1)

        df["target"] = Series()
        df = series_categoriser.label(df, trade_period, stop_percent, limit_percent)

        X = np.zeros((len(df) - trade_period, seq_len, 4))
        Y = np.zeros(len(df) - trade_period)

        for index in range(len(df) - seq_len):
            my_array = np.ndarray((seq_len, 4))
            for j in range(0,seq_len):
                my_array[j] = self.add_row(df, index + j)
            X[index] = preprocessing.scale(my_array)
            Y[index] = df["target"][index + seq_len]

        X = X[:-seq_len]
        Y = np_utils.to_categorical(Y[:-seq_len], 2)

        test_split = int(len(X) * 0.8)

        self.X_train = X[:test_split]
        self.Y_train = Y[:test_split]

        self.X_test = X[test_split:]
        self.Y_test = Y[test_split:]

        self.X_train = self.X_train.reshape(self.X_train.shape[0], 1, seq_len, 4).astype('float32')
        self.X_test = self.X_test.reshape(self.X_test.shape[0], 1, seq_len, 4).astype('float32')
