import lstm
import data_manager
import utils.time_utils
import sys
import numpy as np
from keras.models import model_from_json
import matplotlib.pyplot as plt

def load_model(path, file_name):
    json_file = open('/projects/super-trader-ai/data/training/models/' + file_name + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights('/projects/super-trader-ai/data/training/models/' + file_name + "_model.h5")
    print("Loaded model from disk")
    return loaded_model


def plot_results(predicted_data, true_data, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')

    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')


def plot_prediction(predicted_data, true_data, prediction_len, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    for i, data in enumerate(predicted_data):
        # padding = [None for p in range(len(true_data))]
        plt.plot( data, label='Prediction')

    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')


#Main Run Thread
if __name__=='__main__':
    path = '/projects/super-trader-ai/'
    normalise_window = True
    seq_len = 50

    file_name = sys.argv[1:][0]
    model = load_model(path, file_name)

    start_time = utils.time_utils.timestamp_hours_ago(4.5)

    X_live, y_live, raw_data = data_manager.handle_live_data("BTC_" + file_name.upper(), start_time, seq_len, normalise_window)

    prediction_len = 60
    predictions = lstm.predict_live(model, X_live[-1], seq_len, prediction_len)

    denormalised_prediction = data_manager.denormalise_windows(raw_data, predictions)

    plot_prediction(denormalised_prediction, raw_data, prediction_len, file_name)
