from cnn.candlestick_image_dataloader import loader
import utils.analyse_predictions as ap
import lstm

import numpy as np

from keras.callbacks import ReduceLROnPlateau, EarlyStopping, TensorBoard
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.utils import class_weight
from sklearn.model_selection import GridSearchCV

import sys

# data_length_hours = 24*180
# period = 900
# trade_period = 4
# stop_percent = 3
# limit_percent = 0
# num_classes = 2
# buy_threshold=0.80
# trade_amount=100
# # seq_len = 16
# do_grid_search = False

data_length_hours = 24*150
period = 900
trade_period = 2
stop_percent = 4
limit_percent = 4
num_classes = 2
buy_threshold=0.80
trade_amount=100
# seq_len = 16
do_grid_search = False

def baseline_model(seq_len=16, filters=5, filter_size=5, pool_size=2, dropout_prob=0.3, dense_size=128):
    # create model
    model = Sequential()
    model.add(Conv2D(filters, (filter_size, filter_size), input_shape=(1, seq_len, 500), padding="same", activation='relu', data_format='channels_first'))
    # model.add(Conv2D(filters, (filter_size, filter_size), activation='relu', padding="same"))
    model.add(Conv2D(filters, (filter_size, filter_size), activation='relu', padding="same"))
    model.add(MaxPooling2D(pool_size=(pool_size, pool_size)))
    model.add(Dropout(dropout_prob))

    model.add(Conv2D(filters, (filter_size, filter_size), activation='relu', padding="same"))
    # model.add(Conv2D(filters, (filter_size, filter_size), activation='relu', padding="same"))
    model.add(Conv2D(filters, (filter_size, filter_size), activation='relu', padding="same"))
    model.add(MaxPooling2D(pool_size=(pool_size, pool_size)))
    model.add(Dropout(dropout_prob))

    model.add(Flatten())
    model.add(Dense(dense_size, activation='relu'))
    # model.add(Dense(50, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model

if __name__=='__main__':
    currency = sys.argv[1:][0]
    currency_pair = "BTC_" + currency.upper()

    np.random.seed(2)
    seq_len = 5

    data = loader(currency_pair, seq_len, data_length_hours, period, trade_period, stop_percent, limit_percent)
    X_train, y_train = data.X_train, data.Y_train
    X_test, y_test = data.X_test, data.Y_test
    class_weights = class_weight.compute_class_weight('balanced', np.unique(y_train[:, 0]), y_train[:,0])
    print(class_weights)
    if do_grid_search:
        model = KerasClassifier(build_fn=baseline_model,
            class_weight={0:class_weights[1], 1:class_weights[0]},
            seq_len=seq_len)

        # define the grid search parameters
        batch_size = [10, 20, 40]
        filters = [16, 32, 64, 128, 256]
        filter_size = [2, 3, 4, 6, 8]
        pool_size = [2, 3]
        dense_size = [64, 128, 256, 512]
        dropout_prob = [0.1, 0.2, 0.3, 0.4, 0.5]
        param_grid = dict(filters=filters, filter_size=filter_size)
        grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1)
        grid_result = grid.fit(X_train, y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
    else:
        model = baseline_model(seq_len=seq_len)

        reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.01,
            patience=1, min_lr=0.001, verbose=1)

        earlyStopping = EarlyStopping(monitor='val_loss',
            min_delta=0, patience=10, verbose=0, mode='auto')

        tensorboard = TensorBoard(log_dir='/projects/super-trader-ai/data/training/models/cnn', histogram_freq=1, write_graph=True, write_images=True)

        # ModelCheckpoint(filepath,
        #     monitor='val_loss', verbose=0, save_best_only=False,
        #     save_weights_only=False, mode='auto', period=1)

        # Fit the model
        model.fit(X_train, y_train,
            validation_data=(X_test, y_test),
            epochs=1000,
            batch_size=32,
            verbose=1,
            class_weight={0:class_weights[1], 1:class_weights[0]},
            callbacks=[reduce_lr, earlyStopping, tensorboard])

        lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/cnn/dev_', currency)

        # Final evaluation of the model
        scores = model.evaluate(X_test, y_test, verbose=0)
        print("Baseline Error: %.2f%%" % (100-scores[1]*100))
        print(scores)

        standard_prob = model.predict(X_test, batch_size=100, verbose=1)
        ap.analyse(X_test, y_test, standard_prob, buy_threshold, trade_amount, stop_percent, limit_percent)
        print(standard_prob[-20:])
