import numpy as np
import sys
import lstm
import utils.activation_utils as au

from dataset import loader
import dataset_live
from keras.optimizers import SGD, RMSprop, Adagrad
from keras.models import Sequential, Model
from keras.layers import Input, Merge
from keras.layers.core import Dense, Dropout
from keras.layers.recurrent import LSTM, GRU, SimpleRNN
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix
from keras import backend as K


if __name__=='__main__':
    currency = sys.argv[1:][0]
    currency_pair = "BTC_" + currency.upper()

    batch_size = 500
    maxlen = 25

    # Global params:
    test_split = 0.5
    init_seed = 0
    global_seed = 0
    trade_period = 10
    stop_percent = 2.5
    limit_percent = 1
    train_time_period = 24*60
    test_time_period = 5

    # Load data:
    print("Loading data...")
    dataset = loader(init_seed, train_time_period, maxlen, test_split, currency_pair, trade_period, stop_percent, limit_percent)
    X_train, X_test, Y_train, Y_test = dataset.X_train, dataset.X_test, dataset.Y_train, dataset.Y_test
    X1_train, X1_test = dataset.X1_train, dataset.X1_test
    X2_train, X2_test = dataset.X2_train, dataset.X2_test

    # Set seed:
    np.random.seed(global_seed)

    # Build model:
    print('Build model...')
    timesteps = maxlen
    data_dim = 1

    model1 = Sequential()
    model1.add(LSTM(64, return_sequences=True, input_shape=(timesteps, data_dim)))

    model2 = Sequential()
    model2.add(LSTM(64, return_sequences=True, input_shape=(timesteps, data_dim)))

    model = Sequential()
    model.add(Merge([model1, model2], mode='concat'))
    model.add(Dropout(0.2))
    model.add(LSTM(32, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(8))
    model.add(Dense(2, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    # Train model
    print("Train...")

    model.fit([X_train, X2_train], Y_train, batch_size=batch_size, nb_epoch=20, validation_split=0.2, shuffle=True, class_weight={0:1., 1:5.5})

    lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/stacked_lstm/', currency)

    standard_prob = model.predict([X_test, X2_test], batch_size=batch_size, verbose=1)

    print("Results...")

    predictions = []
    actuals = []
    ml_data_input = []
    correct_predictions = 0
    correct_buy_predictions = 0
    actual_buys = 0
    predicted_buys = 0
    bad_buy_predictions = 0
    for index in range(len(X_test)):
        # prediction = np.argmax(standard_prob[index])
        prediction = au.xover(standard_prob[index], 0.8)
        actual = np.argmax(Y_test[index])
        # print(standard_prob[index])

        actuals.append(actual)
        predictions.append(prediction)

        if prediction == actual:
            correct_predictions = correct_predictions + 1
        if actual == 1:
            actual_buys = actual_buys + 1
        if prediction == 1:
            predicted_buys = predicted_buys + 1
        if prediction == 1 and actual == 1:
            correct_buy_predictions = correct_buy_predictions + 1
        if prediction == 1 and actual == 0:
            bad_buy_predictions = bad_buy_predictions + 1
        e = np.append(dataset.X_test[index], [prediction, actual])
        ml_data_input.append(e)

    percentage_correct = (float(correct_predictions) / float(len(X_test))) * 100.0
    percent_correct_buy = (float(correct_buy_predictions) / float(actual_buys)) * 100.0
    percent_bad_buy = (float(bad_buy_predictions) / float(actual_buys)) * 100.0

    print('{:>20}{:>20}{:>20}{:>20}'.format('Total Predictions', 'Correct Pred %', 'Correct Buy %', 'Incorrect Buy %'))
    print('{:20d}{:20.2f}{:20.2f}{:20.2f}'.format(len(X_test), percentage_correct, percent_correct_buy, percent_bad_buy))

    trade_amount = 5.00
    win = float(correct_buy_predictions * (trade_amount * ((stop_percent-0.5)/100.0)))
    lose = float(bad_buy_predictions * (trade_amount * ((limit_percent+0.5)/100.0)))
    profit = win - lose

    print(confusion_matrix(actuals, predictions))

    print('{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}'.format('Buy predictions', 'Actual Buys', 'Good', 'Bad', 'Profit', 'Loss', 'Balance'))
    print('{:15d}{:15d}{:15d}{:15d}{:15.2f}{:15.2f}{:15.2f}'.format(predicted_buys, actual_buys, correct_buy_predictions, bad_buy_predictions, win, lose, profit))

    np.savetxt('/projects/super-trader-ai/data/prediction/test.csv', ml_data_input, delimiter=',')
