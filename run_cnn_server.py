import datetime
import time
from scipy import signal
import utils.time_utils
import utils.buy_detection as buy_detection
import utils.sell_detection as sell_detection
import data_manager
from keras.models import model_from_json
import matplotlib.pyplot as plt
import cnn.candlestick_image_dataloader
import lstm
import numpy as np
import server.websocket_handler
import server.websocket_client
from cnn.candlestick_image_dataloader import loader

import tornado.httpserver
import tornado.ioloop
import tornado.web

model_path = '/projects/super-trader-ai/data/training/models/cnn/'
data_length_hours = 20 # hours
period = 900
trade_period = 4
stop_percent = 3
limit_percent = 0
models = dict()
init_seed = 0
# currencies = ['ardr', 'burst', 'dash', 'dgb', 'etc', 'fct', 'game', 'gnt', 'huc', 'lbc', 'naut', 'nxt', 'omni', 'pink', 'sc', 'strat', 'xcp', 'xem', 'xpm', 'zec']
# currencies = ['ardr', 'burst', 'dash', 'dgb', 'etc', 'fct', 'game', 'gnt', 'huc', 'lbc', 'naut', 'nxt', 'omni', 'pink', 'sc', 'strat', 'xpm']
currencies = ['dgb', 'ltc', 'game', 'fct', 'fldc']



def make_prediction(seq_len, currency):
    model = models[currency]
    currency_pair = 'BTC_' + currency.upper()

    print("Loading data...")
    dataset = loader(currency_pair, seq_len, data_length_hours, period, trade_period, stop_percent, limit_percent)
    X_test = dataset.X_test

    standard_prob = model.predict([X_test], batch_size=1, verbose=0)
    print(currency)
    print(standard_prob)

    buy_trigger = buy_detection.check_for_buy_trigger(standard_prob, 0.70)
    if buy_trigger:
        print("Buy")
        broadcast_prediction("prediction:buy", currency_pair, standard_prob.tolist())

    sell_trigger = sell_detection.check_for_sell_trigger(standard_prob, 0.40)
    if sell_trigger:
        print("Sell")
        broadcast_prediction("prediction:sell", currency_pair, standard_prob.tolist())


def broadcast_prediction(prediction, currency, predictions):
    data = dict(event=prediction, payload={'currency': currency, 'predictions': predictions})
    server.websocket_handler.webSocketSendMessage(data)


def load_model(currency):
    json_file = open(model_path + currency + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(model_path + currency + "_model.h5")
    print(currency + ": Loaded model from disk")
    return loaded_model


def run_prediction():
    print("Starting Predictions")
    print(datetime.datetime.now().time())

    seq_len = 50

    for currency in currencies:
        make_prediction(seq_len, currency)

    print("Ending Predictions")
    print(datetime.datetime.now().time())


def load_models():
    for currency in currencies:
        models[currency] = load_model(currency)


if __name__=='__main__':
    interval = 0.5 * 60 * 1000

    load_models()

    client = server.websocket_client.WSClient()

    application = tornado.web.Application([
    (r'/ws', server.websocket_handler.WebSocketHandler),
    ])

    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)

    tornado.ioloop.PeriodicCallback(run_prediction, interval).start()
    tornado.ioloop.IOLoop.instance().start()
