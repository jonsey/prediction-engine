import os
import time
import warnings
import json
import numpy as np
from numpy import newaxis
from keras.models import model_from_json
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

def build_model(layers):
    model = Sequential()

    model.add(LSTM(
        input_dim=layers[0],
        output_dim=layers[1],
        return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(
        layers[2],
        return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(
        output_dim=layers[3]))
    model.add(Activation("linear"))

    start = time.time()
    model.compile(loss="mse", optimizer="rmsprop")
    print("> Compilation Time : ", time.time() - start)
    return model

def predict_sequence_full(model, data, window_size):
    #Shift the window by 1 new prediction each time, re-run predictions on new window
    curr_frame = data[0]
    predicted = []
    for i in range(len(data)):
        predicted.append(model.predict(curr_frame[newaxis,:,:])[0,0])
        curr_frame = curr_frame[1:]
        curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
    return predicted


def predict_multiple_continuous(model, data, prediction_len):
    #Predict sequence of 50 steps before shifting prediction run forward by 1 step
    prediction_seqs = []
    for i in range(len(data)):
        curr_frame = data[i]
        predicted = []
        for j in range(prediction_len):
            prediction = model.predict(curr_frame[newaxis,:,:])
            predicted.append(prediction[0,0])
            curr_frame = curr_frame[1:]
            curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
        prediction_seqs.append(predicted)
    return prediction_seqs


def predict_sequences_multiple(model, data, window_size, prediction_len):
    #Predict sequence of 50 steps before shifting prediction run forward by 50 steps
    prediction_seqs = []
    for i in range(int(len(data)/prediction_len)):
        curr_frame = data[i*prediction_len]
        predicted = []
        for j in range(prediction_len):
            prediction = model.predict(curr_frame[newaxis,:,:])
            predicted.append(prediction[0,0])
            curr_frame = curr_frame[1:]
            curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
        prediction_seqs.append(predicted)
    return prediction_seqs

def predict_live(model, data, window_size, prediction_len):
    # data = data[(-1 * prediction_len) - 1:]
    # curr_frame = data[prediction_len]
    # curr_frame = data[-1]
    curr_frame = data

    prediction_seqs = []
    predicted = []
    for j in range(prediction_len):
        prediction = model.predict(curr_frame[newaxis,:,:])
        # print(prediction)
        predicted.append(prediction[0,0])
        curr_frame = curr_frame[1:]
        curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
    prediction_seqs.append(predicted)
    return prediction_seqs

def save_model_and_weights(model, path, file_name):
    # serialize model to JSON
    model_json = model.to_json()
    with open(path + file_name + "model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(path + file_name + "_model.h5")
    print("Saved model to disk")
