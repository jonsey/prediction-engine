import numpy as np
import sys
import lstm
import utils.activation_utils as au

from dataset import loader
import dataset_live
from keras.wrappers.scikit_learn import KerasClassifier
from keras.optimizers import SGD, RMSprop, Adagrad
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, TensorBoard
from keras.models import Sequential, Model
from keras.layers import Input, Merge
from keras.layers.core import Dense, Dropout
from keras.layers.recurrent import LSTM, GRU, SimpleRNN
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from keras import backend as K
from sklearn.utils import class_weight

def build_model(timesteps, data_dim):
    model1 = Sequential()
    model1.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model1.add(Dropout(0.4))

    model2 = Sequential()
    model2.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model2.add(Dropout(0.4))

    model3 = Sequential()
    model3.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model3.add(Dropout(0.4))

    model6 = Sequential()
    model6.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model6.add(Dropout(0.4))

    model7 = Sequential()
    model7.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model7.add(Dropout(0.4))

    model8 = Sequential()
    model8.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model8.add(Dropout(0.3))

    model9 = Sequential()
    model9.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model9.add(Dropout(0.3))

    model10 = Sequential()
    model10.add(LSTM(100, return_sequences=True, input_shape=(timesteps, data_dim)))
    model10.add(Dropout(0.3))

    model = Sequential()
    model.add(Merge([model1, model2, model3, model6, model7, model8, model9, model10], mode='concat'))

    model1.add(LSTM(64, return_sequences=True))
    model2.add(LSTM(64, return_sequences=True))
    model3.add(LSTM(64, return_sequences=True))
    model6.add(LSTM(64, return_sequences=True))
    model7.add(LSTM(64, return_sequences=True))
    model8.add(LSTM(64, return_sequences=True))
    model9.add(LSTM(64, return_sequences=True))
    model10.add(LSTM(64, return_sequences=True))
    model.add(LSTM(64, return_sequences=False))

    model.add(Dense(64, activation='relu'))
    # model.add(Dense(64, activation='relu'))
    # model.add(Dense(64, activation='relu'))
    model.add(Dense(2, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    return model

def build_model_old(timesteps, data_dim):
    model = Sequential()
    model.add(LSTM(240, return_sequences=False, input_shape=(timesteps, data_dim)))
    model.add(Dropout(0.3))
    # model.add(LSTM(100, return_sequences=True))
    # model.add(Dropout(0.3))
    # model.add(LSTM(50, return_sequences=False))
    # model.add(Dropout(0.3))
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    return model

def fnGridSearch(class_weights, seq_len, X_train, y_train):
    model = KerasClassifier(build_fn=build_model,
        class_weight={0:class_weights[1], 1:class_weights[0]})

    # define the grid search parameters
    batch_size = [16, 32, 64]
    units = [32, 64, 128, 256]
    dropout_prob = [0.1, 0.2, 0.3, 0.4, 0.5]
    param_grid = dict(batch_size=batch_size, units=units)
    grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1)
    grid_result = grid.fit(X_train, y_train)
    # summarize results
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))

if __name__=='__main__':
    currency = sys.argv[1:][0]
    currency_pair = "BTC_" + currency.upper()

    grid_search = False
    batch_size = 32
    seq_len = 25

    # Global params:
    test_split = 0.2
    init_seed = 0
    global_seed = 0
    trade_period = 4
    stop_percent = 3.5
    limit_percent = 1.5
    train_time_period = 24*100
    test_time_period = 4

    # Load data:
    print("Loading data...")
    dataset = loader(init_seed, train_time_period, seq_len, test_split, currency_pair, trade_period, stop_percent, limit_percent)
    X_train, X_test, Y_train, Y_test = dataset.X_train, dataset.X_test, dataset.Y_train, dataset.Y_test
    X1_train, X1_test = dataset.X1_train, dataset.X1_test
    X2_train, X2_test = dataset.X2_train, dataset.X2_test
    X3_train, X3_test = dataset.X3_train, dataset.X3_test
    X6_train, X6_test = dataset.X6_train, dataset.X6_test
    X7_train, X7_test = dataset.X7_train, dataset.X7_test
    X8_train, X8_test = dataset.X8_train, dataset.X8_test
    X9_train, X9_test = dataset.X9_train, dataset.X9_test
    X10_train, X10_test = dataset.X10_train, dataset.X10_test

    # Set seed:
    np.random.seed(global_seed)

    # Build model:
    print('Build model...')
    timesteps = seq_len
    data_dim = 1

    class_weights = class_weight.compute_class_weight('balanced', np.unique(Y_train[:, 0]), Y_train[:, 0])
    print(class_weights)

    if(grid_search):
        model = fnGridSearch(class_weights, seq_len, [X_train, X1_train, X2_train, X6_train, X7_train, X8_train, X9_train, X10_train], Y_train)
    else:
        model = build_model(timesteps, data_dim)

        # Train model
        print("Train...")

        reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.01,
            patience=3, min_lr=0.001, verbose=1)

        earlyStopping = EarlyStopping(monitor='val_loss',
            min_delta=0, patience=10, verbose=0, mode='auto')

        input_arr = np.concatenate((X_train, X1_train, X2_train, X6_train, X7_train, X8_train, X9_train, X10_train), axis=2)
        print(np.shape(input_arr))
        model.fit([X_train, X1_train, X2_train, X6_train, X7_train, X8_train, X9_train, X10_train],
            Y_train,
            batch_size=batch_size,
            nb_epoch=500,
            validation_split=0.2,
            shuffle=True,
            class_weight={0:class_weights[1], 1:class_weights[0]},
            callbacks=[reduce_lr, earlyStopping])

        lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/stacked_lstm/', currency)

        test_input_arr = np.concatenate((X_test, X1_test, X2_test, X6_test, X7_test, X8_test, X9_test, X10_test), axis=2)
        standard_prob = model.predict([X_test, X1_test, X2_test, X6_test, X7_test, X8_test, X9_test, X10_test], batch_size=batch_size, verbose=1)

        print("Results...")

        predictions = []
        actuals = []
        ml_data_input = []
        correct_predictions = 0
        correct_buy_predictions = 0
        actual_buys = 0
        predicted_buys = 0
        bad_buy_predictions = 0
        for index in range(len(X_test)):
            # prediction = np.argmax(standard_prob[index])
            prediction = au.xover(standard_prob[index], 0.8)
            actual = np.argmax(Y_test[index])
            # print(standard_prob[index])

            actuals.append(actual)
            predictions.append(prediction)

            if prediction == actual:
                correct_predictions = correct_predictions + 1
            if actual == 1:
                actual_buys = actual_buys + 1
            if prediction == 1:
                predicted_buys = predicted_buys + 1
            if prediction == 1 and actual == 1:
                correct_buy_predictions = correct_buy_predictions + 1
            if prediction == 1 and actual == 0:
                bad_buy_predictions = bad_buy_predictions + 1
            e = np.append(dataset.X_test[index], [prediction, actual])
            ml_data_input.append(e)

        print("Actual Buys")
        print(actual_buys)

        percentage_correct = (float(correct_predictions) / float(len(X_test))) * 100.0
        percent_correct_buy = (float(correct_buy_predictions) / float(actual_buys)) * 100.0
        percent_bad_buy = (float(bad_buy_predictions) / float(actual_buys)) * 100.0

        print('{:>20}{:>20}{:>20}{:>20}'.format('Total Predictions', 'Correct Pred %', 'Correct Buy %', 'Incorrect Buy %'))
        print('{:20d}{:20.2f}{:20.2f}{:20.2f}'.format(len(X_test), percentage_correct, percent_correct_buy, percent_bad_buy))

        trade_amount = 5.00
        win = float(correct_buy_predictions * (trade_amount * ((stop_percent-0.5)/100.0)))
        lose = float(bad_buy_predictions * (trade_amount * ((limit_percent+0.5)/100.0)))
        profit = win - lose

        print(confusion_matrix(actuals, predictions))

        print('{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}'.format('Buy predictions', 'Actual Buys', 'Good', 'Bad', 'Profit', 'Loss', 'Balance'))
        print('{:15d}{:15d}{:15d}{:15d}{:15.2f}{:15.2f}{:15.2f}'.format(predicted_buys, actual_buys, correct_buy_predictions, bad_buy_predictions, win, lose, profit))

        np.savetxt('/projects/super-trader-ai/data/prediction/test.csv', ml_data_input, delimiter=',')
