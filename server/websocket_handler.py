import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket

import json

liveWebSockets = set()

class WebSocketHandler(tornado.websocket.WebSocketHandler):

    def initialize(self):
        print("WS Server initialised")

    def check_origin(self, origin):
        return True

    def open(self):
        print 'new connection'
        liveWebSockets.add(self)
        self.write_message("Prediction Engine Connection Opened")

    def on_message(self, message):
        print message

    def on_close(self):
        print 'connection closed'

def webSocketSendMessage(message):
    removable = set()
    for ws in liveWebSockets:
        if not ws.ws_connection or not ws.ws_connection.stream.socket:
            removable.add(ws)
        else:
            ws.write_message(json.dumps(message))
    for ws in removable:
        liveWebSockets.remove(ws)
