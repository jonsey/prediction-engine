import websocket

class WSClient():

    def __init__(self):
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp("ws://localhost:4000/socket",
                                        on_message = self.on_message,
                                        on_error = self.on_error,
                                        on_close = self.on_close)

        self.ws.on_open = self.on_open
        self.ws.run_forever()

    def on_message(self, ws, message):
        print("received message")
        print(message)

    def on_error(self, ws, error):
        print error

    def on_close(self, ws):
        print "connection closed"

    def on_open(self, ws):
        data = dict(topic="ticker:lobby", event="phx_join", payload={}, ref=None)
        print(data)
        ws.send(data)
        print "connected"

if __name__ == "__main__":
    client = WSClient()
