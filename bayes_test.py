import unittest
from types import *
from ga import genetic
from ga.chromosome import Chromosome, Gene
from bayesian_cost import train

class TestBayes(unittest.TestCase):

    def setUp(self):

        self.cost_function = train
        size_of_pop = 10
        target = 100 # percent profit
        params =  {
            'seq_len': {'val': 14, 'min': 10, 'max': 25, 'type': IntType},
            'trade_period': {'val': 5, 'min': 1, 'max': 10, 'type': IntType},
            'stop_percent': {'val': 4, 'min': 1, 'max': 5, 'type': IntType},
            'limit_percent': {'val': 4, 'min': 0, 'max': 5, 'type': IntType},
            'train_time_period': {'val': 47, 'min': 30, 'max': 60, 'type': IntType}
        }
        p = genetic.population(size_of_pop, len(params), params)

        for i in xrange(3):
            print('Epoch: {}'.format(i))
            p = genetic.evolve('dgb', self.cost_function, p, target)

    def test_initial_fitness(self):
        # print(self.fitness_history)
        pass


if __name__ == '__main__':
    unittest.main()
