import os
import sys
import time
import numpy as np
from keras.callbacks import Callback
from keras.models import Model, Sequential
from keras.layers import Convolution1D, AtrousConvolution1D, Merge, LSTM, Flatten, Dense, Input, Lambda, merge, Activation

def wavenetBlock(n_atrous_filters, atrous_filter_size, atrous_rate):
    def f(input_):
        residual = input_
        tanh_out = AtrousConvolution1D(n_atrous_filters, atrous_filter_size,
                                       atrous_rate=atrous_rate,
                                       border_mode='same',
                                       activation='tanh')(input_)
        sigmoid_out = AtrousConvolution1D(n_atrous_filters, atrous_filter_size,
                                          atrous_rate=atrous_rate,
                                          border_mode='same',
                                          activation='sigmoid')(input_)
        merged = merge([tanh_out, sigmoid_out], mode='mul')
        skip_out = Convolution1D(1, 1, activation='relu', border_mode='same')(merged)
        out = merge([skip_out, residual], mode='sum')
        return out, skip_out
    return f

def build_model(input_size, data_dim):
    input1_ = Input(shape=(input_size, 64))
    input2_ = Input(shape=(input_size, 64))
    input3_ = Input(shape=(input_size, 64))
    merged = merge([input1_, input2_, input3_])
    A, B = wavenetBlock(64, 2, 2)(merged)
    skip_connections = [B]
    for i in range(20):
        A, B = wavenetBlock(64, 2, 2**((i+2)%9))(A)
        skip_connections.append(B)
    net = merge(skip_connections, mode='sum')
    net = Activation('relu')(net)
    net = Convolution1D(1, 1, activation='relu')(net)
    net = Convolution1D(1, 1)(net)
    net = Flatten()(net)
    net = Dense(2, activation='softmax')(net)
    model = Model(input=[input1_, input2_, input3_], output=net)
    model.compile(loss='categorical_crossentropy', optimizer='sgd',
                  metrics=['accuracy'])
    model.summary()
    return model
