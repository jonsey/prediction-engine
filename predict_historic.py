import lstm
import data_manager
import utils.time_utils
import sys
import numpy as np
from keras.models import model_from_json
import matplotlib.pyplot as plt
import talib


def load_model(path, file_name):
    json_file = open('/projects/super-trader-ai/data/training/models/' + file_name + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights('/projects/super-trader-ai/data/training/models/' + file_name + "_model.h5")
    print("Loaded model from disk")
    return loaded_model

def plot_results_multiple(predicted_data, true_data, prediction_len, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    #Pad the list of predictions to shift it in the graph to it's correct start
    for x, prediction in enumerate(predicted_data):
        for i, data in enumerate(prediction):
            padding = [None for p in range(x + prediction_len)]
            plt.plot(padding + data, label='Prediction')

    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')

def plot_results(predicted_data, true_data, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')

    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')


def plot_prediction(predicted_data, true_data, prediction_len, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(len(true_data))]
        plt.plot( padding + data, label='Prediction')

    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')

def calculate_change_over_period_into_future(raw_data, current_index, period):
    change = raw_data[current_index + period] - raw_data[index]
    return (change / raw_data[index-1])*100

def append_percent_increase_cols(ml_data_input, raw_data, index, period):
    percent_increase = calculate_change_over_period_into_future(raw_data, index, period)
    return np.append(ml_data_input, percent_increase)


def check_percent_increase(ml_data_input, raw_data, index, period):
    percent_increase = calculate_change_over_period_into_future(raw_data, index, period)
    return percent_increase > 3

def check_oversold(close):
    data = np.array(close, np.float)
    fastk, fastd = talib.STOCHRSI(data, timeperiod=14, fastk_period=5, fastd_period=3, fastd_matype=0)
    plot_results(fastd, fastk, "STOCHRSI")
    np.savetxt('/projects/super-trader-ai/data/prediction/stochrsi.csv', fastd, delimiter=',')
    return fastd



#Main Run Thread
if __name__=='__main__':
    path = '/projects/super-trader-ai/'
    normalise_window = True
    seq_len = 50

    file_name = sys.argv[1:][0]
    model = load_model(path, file_name)

    start_time = utils.time_utils.timestamp_hours_ago(9*1)

    X_live, y_live, raw_data = data_manager.handle_live_data("BTC_" + file_name.upper(), start_time, seq_len, normalise_window)

    prediction_len = 50

    predictions = []
    ml_data_input = []
    stochrsi = check_oversold(raw_data)

    for index in range(len(X_live)):
        is_buy = False
        prediction = lstm.predict_live(model, X_live[index], seq_len, prediction_len)
        denormalised_prediction = data_manager.denormalise_windows(raw_data, prediction)
        predictions.append(denormalised_prediction)
        c = np.append(denormalised_prediction, raw_data[index])
        d = np.append(c, stochrsi[index])
        for x in range(1, 11):
            d = append_percent_increase_cols(d, raw_data, index, x)
            if(check_percent_increase(d, raw_data, index, x) and (stochrsi[index] <= 20)):
                is_buy = True

        if(is_buy):
            plot_prediction(denormalised_prediction, raw_data[:index], prediction_len, file_name + '_buy_' + str(index))
        else:
            plot_prediction(denormalised_prediction, raw_data[:index], prediction_len, file_name + str(index))

        e = np.append(d, is_buy)
        ml_data_input.append(e)



    np.savetxt('/projects/super-trader-ai/data/prediction/test.csv', ml_data_input, delimiter=',')



    # plot_results_multiple(predictions, raw_data[(-1 * len(raw_data)):], prediction_len, file_name)
