FROM thoughtram/keras

run apt-get update
run apt-get -y install build-essential
run apt-get -y install wget
run apt-get -y install git
run apt-get -y install python-pydot
run apt-get -y install graphviz


RUN pip install --upgrade pip

RUN pip install tensorflow --upgrade
RUN pip install h5py
RUN pip install requests
RUN pip install tornado
RUN pip install websocket-client
RUN pip install twisted
RUN pip install -U memory_profiler
RUN pip install pandas
RUN pip install pandas-ml
RUN pip install sklearn
RUN pip install -U imbalanced-learn
RUN pip install gym
RUN pip install keras --upgrade

# TA-Lib
RUN wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz && \
  tar -xvzf ta-lib-0.4.0-src.tar.gz && \
  cd ta-lib/ && \
  ./configure --prefix=/usr && \
  make && \
  make install
run pip install TA-Lib
