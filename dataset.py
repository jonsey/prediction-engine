import numpy as np
from numpy import newaxis
from keras.preprocessing import sequence
from keras.utils.np_utils import to_categorical
from pandas import read_json
from pandas import datetime
from pandas import DataFrame
from pandas import concat
from pandas import Series
import requests
from sklearn import preprocessing
import utils.ta_utils as ta
import utils.panda_file as pf
import math

import pandas_ml as pdml

import utils.time_utils

class loader(object):

    	def __init__(self, init_seed, time_period, maxlen, test_split, currency_pair, trade_period, stop_percent, limit_percent):
            start_time = utils.time_utils.timestamp_hours_ago(time_period)
            period = 900
            response = requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=' + currency_pair + '&start='+ str(start_time) +'&end=9999999999&period=' + str(period))

            series = read_json(response.text, orient='records')
            series = series.set_index(['date'])
            series = series.drop('weightedAverage', axis=1)
            # series = series.drop('high', axis=1)
            # series = series.drop('low', axis=1)
            # series = series.drop('open', axis=1)
            series = series.drop('volume', axis=1)
            series["target"] = Series()

            # series = ta.create_stochrsi_series(series)
            series = ta.create_macd_series(series)
            series = ta.create_bb_series(series, "bb")
            # series = ta.create_ma_series(series, 5, "ma")

            y = (i for i,v in enumerate(series["macd"].values) if math.isnan(v))
            start_index = sum(1 for _ in y)
            series = series[start_index:]

            pf.save_pandas('/projects/super-trader-ai/data/training/models/stacked_lstm/' + currency_pair + '_raw.dat', series)
            # series = pf.load_pandas('/projects/super-trader-ai/data/training/models/stacked_lstm/' + currency_pair + '_raw.dat', 'c')

            #label data
            for index, close in enumerate(series["close"][:-trade_period]):
                _hit_limit_without_hiting_stop = self._price_increased_by(trade_period, series, close, index, stop_percent, limit_percent)
                _hit_stop_without_hiting_limit = self._price_decreased_by(trade_period, series, close, index, stop_percent, limit_percent)

                if _hit_limit_without_hiting_stop:
                    series["target"][index] = 1
                elif _hit_stop_without_hiting_limit:
                    series["target"][index] = 0
                else:
                    series["target"][index] = 0

            series = series[:-trade_period]

            series.to_csv('/projects/super-trader-ai/data/df.csv')

            X = series["close"].values
            X1 = series["quoteVolume"].values
            X2 = series["macd"].values
            X3 = series["open"].values
            X4 = series["high"].values
            X5 = series["low"].values
            X6 = series["bb_upper"].values
            X7 = series["bb_middle"].values
            X8 = series["bb_lower"].values
            X9 = series["bb_width"].values
            X10 = series["bb_percent"].values
            Y = to_categorical(series["target"].values, 2)

            new_X = []
            new_X1 = []
            new_X2 = []
            new_X3 = []
            new_X4 = []
            new_X5 = []
            new_X6 = []
            new_X7 = []
            new_X8 = []
            new_X9 = []
            new_X10 = []
            new_Y = []
            for index in range(len(X) - maxlen):
                new_X.append(X[index: index + maxlen])
                new_X1.append(X1[index: index + maxlen])
                new_X2.append(X2[index: index + maxlen])
                new_X3.append(X3[index: index + maxlen])
                new_X4.append(X4[index: index + maxlen])
                new_X5.append(X5[index: index + maxlen])
                new_X6.append(X6[index: index + maxlen])
                new_X7.append(X7[index: index + maxlen])
                new_X8.append(X8[index: index + maxlen])
                new_X9.append(X9[index: index + maxlen])
                new_X10.append(X10[index: index + maxlen])
                # new_Y.append(Y[index + maxlen + maxlen - 1])



            X = np.array(new_X)
            X1 = np.array(new_X1)
            X2 = np.array(new_X2)
            X3 = np.array(new_X3)
            X4 = np.array(new_X4)
            X5 = np.array(new_X5)
            X6 = np.array(new_X6)
            X7 = np.array(new_X7)
            X8 = np.array(new_X8)
            X9 = np.array(new_X9)
            X10 = np.array(new_X10)
            Y = Y[maxlen:]

            np.savetxt('/projects/super-trader-ai/data/x.csv', X, delimiter=',')
            np.savetxt('/projects/super-trader-ai/data/y.csv', Y, delimiter=',')

            split_index = int(len(X)*(1-test_split))

            self.X_train = preprocessing.scale(X[:split_index])
            self.X1_train = preprocessing.scale(X1[:split_index])
            self.X2_train = preprocessing.scale(X2[:split_index])
            self.X3_train = preprocessing.scale(X3[:split_index])
            self.X4_train = preprocessing.scale(X4[:split_index])
            self.X5_train = preprocessing.scale(X5[:split_index])
            self.X6_train = preprocessing.scale(X6[:split_index])
            self.X7_train = preprocessing.scale(X7[:split_index])
            self.X8_train = preprocessing.scale(X8[:split_index])
            self.X9_train = preprocessing.scale(X9[:split_index])
            self.X10_train = preprocessing.scale(X10[:split_index])
            self.Y_train = Y[:split_index]

            self.X_train = np.reshape(self.X_train, (self.X_train.shape[0], self.X_train.shape[1], 1))
            self.X1_train = np.reshape(self.X1_train, (self.X1_train.shape[0], self.X1_train.shape[1], 1))
            self.X2_train = np.reshape(self.X2_train, (self.X2_train.shape[0], self.X2_train.shape[1], 1))
            self.X3_train = np.reshape(self.X3_train, (self.X3_train.shape[0], self.X3_train.shape[1], 1))
            self.X4_train = np.reshape(self.X4_train, (self.X4_train.shape[0], self.X4_train.shape[1], 1))
            self.X5_train = np.reshape(self.X5_train, (self.X5_train.shape[0], self.X5_train.shape[1], 1))
            self.X6_train = np.reshape(self.X6_train, (self.X6_train.shape[0], self.X6_train.shape[1], 1))
            self.X7_train = np.reshape(self.X7_train, (self.X7_train.shape[0], self.X7_train.shape[1], 1))
            self.X8_train = np.reshape(self.X8_train, (self.X8_train.shape[0], self.X8_train.shape[1], 1))
            self.X9_train = np.reshape(self.X9_train, (self.X9_train.shape[0], self.X9_train.shape[1], 1))
            self.X10_train = np.reshape(self.X10_train, (self.X10_train.shape[0], self.X10_train.shape[1], 1))


            self.X_test = preprocessing.scale(X[split_index:])
            self.X1_test = preprocessing.scale(X1[split_index:])
            self.X2_test = preprocessing.scale(X2[split_index:])
            self.X3_test = preprocessing.scale(X3[split_index:])
            self.X4_test = preprocessing.scale(X4[split_index:])
            self.X5_test = preprocessing.scale(X5[split_index:])
            self.X6_test = preprocessing.scale(X6[split_index:])
            self.X7_test = preprocessing.scale(X7[split_index:])
            self.X8_test = preprocessing.scale(X8[split_index:])
            self.X9_test = preprocessing.scale(X9[split_index:])
            self.X10_test = preprocessing.scale(X10[split_index:])
            self.Y_test = Y[split_index:]

            self.X_test = np.reshape(self.X_test, (self.X_test.shape[0], self.X_test.shape[1], 1))
            self.X1_test = np.reshape(self.X1_test, (self.X1_test.shape[0], self.X1_test.shape[1], 1))
            self.X2_test = np.reshape(self.X2_test, (self.X2_test.shape[0], self.X2_test.shape[1], 1))
            self.X3_test = np.reshape(self.X3_test, (self.X3_test.shape[0], self.X3_test.shape[1], 1))
            self.X4_test = np.reshape(self.X4_test, (self.X4_test.shape[0], self.X4_test.shape[1], 1))
            self.X5_test = np.reshape(self.X5_test, (self.X5_test.shape[0], self.X5_test.shape[1], 1))
            self.X6_test = np.reshape(self.X6_test, (self.X6_test.shape[0], self.X6_test.shape[1], 1))
            self.X7_test = np.reshape(self.X7_test, (self.X7_test.shape[0], self.X7_test.shape[1], 1))
            self.X8_test = np.reshape(self.X8_test, (self.X8_test.shape[0], self.X8_test.shape[1], 1))
            self.X9_test = np.reshape(self.X9_test, (self.X9_test.shape[0], self.X9_test.shape[1], 1))
            self.X10_test = np.reshape(self.X10_test, (self.X10_test.shape[0], self.X10_test.shape[1], 1))

            print(len(self.X_train), 'train sequences', np.shape(self.X_train))
            print(len(self.Y_train), 'train targets', np.shape(self.Y_train))
            print(len(self.X_test), 'test sequences', np.shape(self.X_test))
            print(len(self.Y_test), 'test targets', np.shape(self.Y_test))

            print(self.Y_test)


        def _price_increased_by(self, trade_period, series, starting_close, current_index, stop_percent, limit_percent):
            _required_rise = starting_close * (1.0 + (stop_percent/100.0))
            _required_stop_loss = starting_close - (starting_close * (limit_percent/100.0))

            for x in range(1, trade_period + 1):
                if(series["high"][x + current_index] >= _required_rise):
                    return True
                if(series["low"][x + current_index] <= _required_stop_loss):
                    return False

            return False

        def _price_decreased_by(self, trade_period, series, starting_close, current_index, stop_percent, limit_percent):
            _required_rise = starting_close * (1.0 + (stop_percent/100.0))
            _required_stop_loss = starting_close - (starting_close * (limit_percent/100.0))

            for x in range(1, trade_period + 1):
                if(series["low"][x + current_index] <= _required_stop_loss):
                    return True

            return False
