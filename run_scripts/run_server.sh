docker run -it --rm \
  -v ~/code/docker/super-trader-ai:/projects/super-trader-ai \
  -p 8888:8888 \
  amrsoftware/keras-tf-tornado-py2 \
  python /projects/super-trader-ai/run_server.py
