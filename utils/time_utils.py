import time
import datetime

def timestamp_hours_ago(time_in_hours):
    start_time = datetime.datetime.now() - datetime.timedelta(hours=time_in_hours)
    return time.mktime(start_time.timetuple())

def timestamp_days_ago(time_in_days):
    start_time = datetime.datetime.now() - datetime.timedelta(days=time_in_days)
    return time.mktime(start_time.timetuple())
