
def check_for_sell_trigger(predictions, threshold):
    isDecreasing = predictions[-1][1] < predictions[-2][1]
    isUnderthreshold = predictions[-1][1] < threshold

    return isDecreasing and isUnderthreshold
