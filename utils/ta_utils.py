from pandas import Series
import talib
import math

def create_stochrsi_series(series):
    fastk, fastd = talib.STOCHRSI(series["close"].values, timeperiod=14, fastk_period=5, fastd_period=3, fastd_matype=0)
    series["stochrsi"] = fastk
    return series

def create_macd_series(series, fastperiod=12, slowperiod=26, signalperiod=9):
    macd, signal, hist = talib.MACD(series["close"].values,
                                    fastperiod=fastperiod,
                                    slowperiod=slowperiod,
                                    signalperiod=signalperiod)

    temp = []
    for i in range(0, len(macd)):
        temp.append(macd[i].astype('float32') - signal[i].astype('float32'))

    series["macd"] = temp
    return series

def create_ma_series(series, period, name):
    output = talib.SMA(series["close"].values, timeperiod=period)
    series[name] = output.astype('float32')
    return series

def create_bb_series(series, name):
    multiplier = (1.0/max(series["close"].values))
    upper, middle, lower = talib.BBANDS(series["close"].values * multiplier, timeperiod=20, nbdevup=2, nbdevdn=2, matype=0)
    series[name + "_upper"] = upper.astype('float32') / multiplier
    series[name + "_middle"] = middle.astype('float32') / multiplier
    series[name + "_lower"] = lower.astype('float32') / multiplier
    series[name + "_width"] =  ((upper.astype('float32') - lower.astype('float32')) / middle.astype('float32')) * 100
    series[name + "_percent"] =  (series["close"].values - lower.astype('float32')) / (upper.astype('float32') - lower.astype('float32')) * 100
    return series
