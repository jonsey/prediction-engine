import utils.activation_utils as au
import numpy as np
from sklearn.metrics import confusion_matrix

def analyse(X_test, Y_test, standard_prob, buy_threshold, trade_amount, stop_percent, limit_percent):
    predictions = []
    actuals = []
    ml_data_input = []
    correct_predictions = 0
    correct_buy_predictions = 0
    actual_buys = 0
    predicted_buys = 0
    bad_buy_predictions = 0

    for index in range(len(Y_test)):

        prediction = au.xover(standard_prob[index], buy_threshold)
        actual = np.argmax(Y_test[index])

        actuals.append(actual)
        predictions.append(prediction)

        if prediction == actual:
            correct_predictions = correct_predictions + 1
        if actual == 1:
            actual_buys = actual_buys + 1
        if prediction == 1:
            predicted_buys = predicted_buys + 1
        if prediction == 1 and actual == 1:
            correct_buy_predictions = correct_buy_predictions + 1
        if prediction == 1 and actual == 0:
            bad_buy_predictions = bad_buy_predictions + 1
        e = np.append(X_test[index], [prediction, actual])
        ml_data_input.append(e)

    data_len = len(Y_test)
    percentage_correct = (float(correct_predictions) / float(data_len)) * 100.0
    percent_correct_buy = (float(correct_buy_predictions) / float(data_len)) * 100.0
    percent_bad_buy = (float(bad_buy_predictions) / float(data_len)) * 100.0

    print("Analysis----------------------------------------------------------------------------------------------------")
    print('{:>20}{:>20}{:>20}{:>20}'.format('Total Predictions', 'Correct Pred %', 'Correct Buy %', 'Incorrect Buy %'))
    print('{:20d}{:20.2f}{:20.2f}{:20.2f}'.format(len(Y_test), percentage_correct, percent_correct_buy, percent_bad_buy))

    trade_amount = trade_amount
    win = float(correct_buy_predictions * (trade_amount * ((stop_percent-0.5)/100.0)))
    lose = float(bad_buy_predictions * (trade_amount * ((limit_percent+0.5)/100.0)))
    profit = win - lose

    print(confusion_matrix(actuals, predictions))

    print('{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}'.format('Buy predictions', 'Actual Buys', 'Good', 'Bad', 'Profit', 'Loss', 'Balance'))
    print('{:15d}{:15d}{:15d}{:15d}{:15.2f}{:15.2f}{:15.2f}'.format(predicted_buys, actual_buys, correct_buy_predictions, bad_buy_predictions, win, lose, profit))

    np.savetxt('/projects/super-trader-ai/data/prediction/test.csv', ml_data_input, delimiter=',')
