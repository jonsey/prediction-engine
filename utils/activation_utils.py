def xover(ts, cut):
    x = ts > cut
    return x.argmax() if x.any() else 0
