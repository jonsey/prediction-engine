
def check_for_buy_trigger(predictions, threshold):
    isIncreasing = predictions[-1][1] > predictions[-2][1]
    isOverThreshold = predictions[-1][1] >= threshold

    return isIncreasing and isOverThreshold
