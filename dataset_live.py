import numpy as np
from numpy import newaxis
from keras.preprocessing import sequence
from keras.utils.np_utils import to_categorical
from pandas import read_json
from pandas import datetime
from pandas import DataFrame
from pandas import concat
from pandas import Series
import requests
from sklearn import preprocessing
import utils.ta_utils as ta
import utils.panda_file as pf
import math

import utils.time_utils

class loader(object):
    	def __init__(self, init_seed, maxlen, currency_pair, time_period):
            start_time = utils.time_utils.timestamp_hours_ago(time_period)
            period = 300

            url = 'https://poloniex.com/public?command=returnChartData&currencyPair=' + currency_pair + '&start='+ str(start_time) +'&end=9999999999&period=' + str(period)
            response = requests.get(url)
            # print(url)

            series = read_json(response.text, orient='records')
            series = series.set_index(["date"])

            print('Number of raw datapoints: {}'.format(len(series)))

            series = ta.create_macd_series(series)

            y = (i for i,v in enumerate(series["macd"].values) if math.isnan(v))
            start_index = sum(1 for _ in y)
            series = series[start_index:]

            X = preprocessing.scale(series["close"].values)
            X1 = preprocessing.scale(series["quoteVolume"].values)
            X2 = preprocessing.scale(series["macd"].values)


            new_X = []
            new_X1 = []
            new_X2 = []

            for index in range(len(X) - maxlen + 1):
                new_X.append(X[index: index + maxlen])
                new_X1.append(X1[index: index + maxlen])
                new_X2.append(X2[index: index + maxlen])

            X = np.array(new_X)
            X1 = np.array(new_X1)
            X2 = np.array(new_X2)

            self.X_test = X
            self.X1_test = X1
            self.X2_test = X2

            self.X_test = np.reshape(self.X_test, (self.X_test.shape[0], self.X_test.shape[1], 1))
            self.X1_test = np.reshape(self.X1_test, (self.X1_test.shape[0], self.X1_test.shape[1], 1))
            self.X2_test = np.reshape(self.X2_test, (self.X2_test.shape[0], self.X2_test.shape[1], 1))

            # print(len(self.X_test), 'test sequences Close', np.shape(self.X_test))
            # print(len(self.X2_test), 'test sequences MACD', np.shape(self.X2_test))
