import os
import time
import datetime
import warnings
import json
import numpy as np
import requests
from numpy import newaxis
from keras.models import model_from_json

def handle_live_data(currency, start_time, seq_len, normalise_window):
    response = requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=' + currency + '&start='+ str(start_time) +'&end=9999999999&period=300')
    raw_data = json.loads(response.text)

    data = []
    for item in raw_data:
        data.append(item["close"])

    sequence_length = seq_len + 1
    result = []
    for index in range(len(data) - sequence_length + 1):
        result.append(data[index: index + sequence_length])
    #
    # print(data)
    # print("Result")
    # print(result)

    if normalise_window:
        result = normalise_windows(result)

    result = np.array(result)


    row = round(result.shape[0])
    live = result[:int(row), :]
    # print(result)
    # print(live)
    # x_live = live[:, :-1]
    x_live = result
    y_live = live[:, -1]

    x_live = np.reshape(x_live, (x_live.shape[0], x_live.shape[1], 1))

    return [x_live, y_live, data]

def handle_training_data(filename, start_time, seq_len, normalise_window):
    currency = filename.upper()

    response = requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=BTC_' + currency + '&start='+ str(start_time) +'&end=9999999999&period=300')
    raw_data = json.loads(response.text)

    data = []
    for item in raw_data:
        data.append(item["close"])

    sequence_length = seq_len + 1
    result = []
    for index in range(len(data) - sequence_length):
        result.append(data[index: index + sequence_length])

    if normalise_window:
        result = normalise_windows(result)

    result = np.array(result)

    row = round(0.9 * result.shape[0])
    train = result[:int(row), :]
    np.random.shuffle(train)
    x_train = train[:, :-1]
    y_train = train[:, -1]
    x_test = result[int(row):, :-1]
    y_test = result[int(row):, -1]

    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
    x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

    return [x_train, y_train, x_test, y_test, data]

def normalise_windows(window_data):
    normalised_data = []
    for window in window_data:
        normalised_window = [((float(p) / float(window[0])) - 1) for p in window]
        normalised_data.append(normalised_window)
    return normalised_data

def denormalise_windows(raw_data, window_data):
    denormalised_data = []
    for window in window_data:
        denormalised_window = [(float(raw_data[0]) * (float(p) + 1)) for p in window]
        denormalised_data.append(denormalised_window)
    return denormalised_data
