import datetime
import time
from scipy import signal
import utils.time_utils
import utils.buy_detection as buy_detection
import utils.sell_detection as sell_detection
import data_manager
from keras.models import model_from_json
import matplotlib.pyplot as plt
import data_manager
import lstm
import numpy as np
import server.websocket_handler

import tornado.httpserver
import tornado.ioloop
import tornado.web

models = dict()
# currencies = ['ardr', 'burst', 'dash', 'dgb', 'etc', 'fct', 'game', 'gnt', 'huc', 'lbc', 'naut', 'nxt', 'omni', 'pink', 'sc', 'strat', 'xcp', 'xem', 'xpm', 'zec']
# currencies = ['ardr', 'burst', 'dash', 'dgb', 'etc', 'fct', 'game', 'gnt', 'huc', 'lbc', 'naut', 'nxt', 'omni', 'pink', 'sc', 'strat', 'xpm']
currencies = ['dgb']


def make_prediction(model_path, seq_len, normalise_window, currency_name):
    model = models[currency_name]
    currency = 'BTC_' + currency_name.upper()

    start_time = utils.time_utils.timestamp_hours_ago(5)

    X_live, y_live, raw_data = data_manager.handle_live_data(currency, start_time, seq_len, normalise_window)

    prediction_len = 10

    predictions = lstm.predict_live(model, X_live[-1], seq_len, prediction_len)
    denormalised_prediction = data_manager.denormalise_windows(raw_data, predictions)

    # buy_trigger = buy_detection.check_for_buy_trigger(denormalised_prediction[0])
    # if buy_trigger:
    #     broadcast_prediction("prediction:buy", currency, denormalised_prediction)
    #     currency += '_buy'
    #
    # sell_trigger = sell_detection.check_for_sell_trigger(denormalised_prediction[0])
    # if sell_trigger:
    #     broadcast_prediction("prediction:sell", currency, denormalised_prediction)
    #     currency += '_sell'

    # if buy_trigger or  sell_trigger:
    plot_file_name = str(datetime.datetime.now()) + '--' + currency
    plot_prediction(denormalised_prediction, raw_data, prediction_len, plot_file_name)



def broadcast_prediction(prediction, currency, predictions):
    data = dict(event=prediction, payload={'currency': currency, 'predictions': predictions})
    server.websocket_handler.webSocketSendMessage(data)


def load_model(path, currency):
    json_file = open('/projects/super-trader-ai/data/training/models/' + currency + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights('/projects/super-trader-ai/data/training/models/' + currency + "_model.h5")
    print("Loaded model from disk")
    return loaded_model


def plot_prediction(predicted_data, true_data, prediction_len, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(len(true_data))]
        plt.plot( padding + data, label='Prediction')

    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')


def run_prediction():
    print("Starting Predictions")
    print(datetime.datetime.now().time())

    model_path = '/projects/super-trader-ai/data/training/models/'
    seq_len = 50
    normalise_window = True

    for currency in currencies:
        make_prediction(model_path, seq_len, normalise_window, currency)

    print("Ending Predictions")
    print(datetime.datetime.now().time())


def load_models():
    model_path = '/projects/super-trader-ai/data/training/models/'
    for currency in currencies:
        models[currency] = load_model(model_path, currency)


if __name__=='__main__':
    interval = 1 * 60 * 1000

    load_models()

    application = tornado.web.Application([
    (r'/ws', server.websocket_handler.WebSocketHandler),
    ])

    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)

    tornado.ioloop.PeriodicCallback(run_prediction, interval).start()
    tornado.ioloop.IOLoop.instance().start()
