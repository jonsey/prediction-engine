from keras.datasets import cifar10 # subroutines for fetching the CIFAR-10 dataset
from keras.models import Model # basic class for specifying and training a neural network
from keras.layers import Input, Convolution1D, MaxPooling1D, Dense, Dropout, Flatten
from keras.utils import np_utils # utilities for one-hot encoding of ground truth values
from keras.callbacks import LearningRateScheduler
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.utils import class_weight
from sklearn.model_selection import GridSearchCV
import numpy as np
import math
import sys

from dataset import loader
from utils import analyse_predictions as ap
import lstm
import cnn.cnn_model as cnn
from cnn.callbacks import FilterActivations

batch_size = 32 # in each iteration, we consider 32 training examples at once
num_epochs = 1 # we iterate 200 times over the entire training set
kernel_size = 3 # we will use 3x3 kernels throughout
pool_size = 2 # we will use 2x2 pooling throughout
conv_depth_1 = 32 # we will initially have 32 kernels per conv. layer...
conv_depth_2 = 64 # ...switching to 64 after the first pooling layer
drop_prob_1 = 0.25 # dropout after pooling with probability 0.25
drop_prob_2 = 0.5 # dropout in the FC layer with probability 0.5
hidden_size = 512 # the FC layer will have 512 neurons


if __name__=='__main__':
    currency = sys.argv[1:][0]
    currency_pair = "BTC_" + currency.upper()

    # batch_size = 500
    maxlen = 100
    global_seed = 0

    # Global params:
    test_split = 0.1
    init_seed = 0
    global_seed = 0
    trade_period = 5
    stop_percent = 1.5
    limit_percent = 0
    train_time_period = 24*30
    buy_threshold = 0.6
    trade_amount = 100

    print("Loading data...")
    dataset = loader(init_seed, train_time_period, maxlen, test_split, currency_pair, trade_period, stop_percent, limit_percent)
    X_train, X_test, Y_train, Y_test = dataset.X_train, dataset.X_test, dataset.Y_train, dataset.Y_test
    X1_train, X1_test = dataset.X1_train, dataset.X1_test
    X2_train, X2_test = dataset.X2_train, dataset.X2_test

    print(X_train.shape)
    num_train, seq_len, depth = X_train.shape
    num_test = X_test.shape[0] # there are 10000 test examples in CIFAR-10
    num_classes = np.unique(Y_train).shape[0]

    # Set seed:
    np.random.seed(global_seed)
    model = cnn.build_model(seq_len, depth, conv_depth_1, kernel_size, pool_size, conv_depth_2, drop_prob_1, drop_prob_2, hidden_size, num_classes)
    # model = KerasClassifier(build_fn=cnn.build_model,
    #     seq_len=seq_len,
    #     depth=depth,
    #     conv_depth_1=conv_depth_1,
    #     kernel_size=kernel_size,
    #     pool_size=pool_size,
    #     conv_depth_2=conv_depth_2,
    #     drop_prob_1=drop_prob_1,
    #     drop_prob_2=drop_prob_2,
    #     hidden_size=hidden_size,
    #     num_classes=num_classes)
    #
    # # define the grid search parameters
    # batch_size = [10, 20, 40, 60, 80, 100]
    # epochs = [10, 50, 100]
    # param_grid = dict(batch_size=batch_size, epochs=epochs)
    # grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1)
    # grid_result = grid.fit(X_train, Y_train)
    # # summarize results
    # print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    # means = grid_result.cv_results_['mean_test_score']
    # stds = grid_result.cv_results_['std_test_score']
    # params = grid_result.cv_results_['params']
    # for mean, stdev, param in zip(means, stds, params):
    #     print("%f (%f) with: %r" % (mean, stdev, param))

    class_weights = class_weight.compute_class_weight('balanced', np.unique(Y_train[:, 0]), Y_train[:,0])
    # class_weight={0:class_weights[1], 1:class_weights[0]},


    callbacks = [FilterActivations()]

    model.fit(X_train, Y_train,                # Train the model using the training set...
          batch_size=batch_size,
          epochs=num_epochs,
          verbose=1,
          class_weight={0:class_weights[1], 1:class_weights[0]},
          validation_split=0.1,
          callbacks=callbacks) # ...holding out 10% of the data for validation

    lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/cnn/test/', currency)

    # history = model.evaluate(X_test, Y_test, verbose=1)  # Evaluate the trained model on the test set!
    standard_prob = model.predict(X_test, batch_size=batch_size, verbose=1)
    ap.analyse(X_test, Y_test, standard_prob, buy_threshold, trade_amount, stop_percent, limit_percent)
    print(standard_prob[-15:])
