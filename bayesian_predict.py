import numpy as np
import sys

import utils.time_utils
from dataset_live import loader
from keras.optimizers import SGD, RMSprop, Adagrad
from keras.models import Sequential, Model, model_from_json
from keras.layers import Input, Merge
from keras.layers.core import Dense, Dropout
from keras.layers.recurrent import LSTM, GRU, SimpleRNN
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix


def load_model(path, file_name):
    json_file = open('/projects/super-trader-ai/data/training/models/stacked_lstm/' + file_name + 'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights('/projects/super-trader-ai/data/training/models/stacked_lstm/' + file_name + "_model.h5")
    print("Loaded model from disk")
    return loaded_model

#Main Run Thread
if __name__=='__main__':
    path = '/projects/super-trader-ai/'
    batch_size = 500
    init_seed = 0
    maxlen = 11
    predict_time_period = 30*24

    currency = sys.argv[1:][0]
    currency_pair = "BTC_" + currency.upper()
    model = load_model(path, currency)

    print("Loading data...")
    dataset = loader(init_seed, maxlen, currency_pair, predict_time_period)
    X_test, X1_test, X2_test = dataset.X_test, dataset.X1_test, dataset.X2_test

    standard_prob = model.predict([X_test, X1_test, X2_test], batch_size=batch_size, verbose=1)
    print(standard_prob)
    prediction = np.argmax(standard_prob[-1])
    print(prediction)
