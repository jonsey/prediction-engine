import lstm
import data_manager
import utils.time_utils
import time
import sys
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


def plot_results_multiple(predicted_data, true_data, prediction_len, predictions_file_name):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    #Pad the list of predictions to shift it in the graph to it's correct start
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(i * prediction_len)]
        plt.plot(padding + data, label='Prediction')
        # plt.legend()
    # plt.show()
    fig.savefig('/projects/super-trader-ai/data/prediction/' + predictions_file_name + '.png')


# def train_model(X_train, y_train, raw_data, [h1, h2, epochs, batch_size, seq_len, prediction_len]):
#     # model = lstm.build_model([1, 100, 50, 1])
#     try:
#         model = lstm.build_model([1, 50, 5, 1])
#
#         model.fit(
#             X_train,
#             y_train,
#             batch_size=500,
#             nb_epoch=epochs,
#             validation_split=0.4)
#
#         lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/', file_name)
#
#         predictions, expected = lstm.predict_sequences_multiple(model, X_test, seq_len, prediction_len)
#
#         rmse = sqrt(mean_squared_error(expected, predictions))
#         print('Test RMSE: %.3f' % rmse)
#
#         print('Training duration (s) : ', time.time() - global_start_time)
#         plot_results_multiple(predictions, y_test, prediction_len, file_name)
#         return rmse
#     except Exception as e:
#         return 100


#Main Run Thread
if __name__=='__main__':
    global_start_time = time.time()
    epochs  = 1
    seq_len = 25
    prediction_len = 25
    file_name = sys.argv[1:][0]
    start_time = utils.time_utils.timestamp_days_ago(30)

    print('> Loading data... ')

    X_train, y_train, X_test, y_test, raw_data = data_manager.handle_training_data(file_name, start_time, seq_len, True)

    print('> Data Loaded. Compiling...')

    model = lstm.build_model([1, 100, 50, 1])

    model.fit(
        X_train,
        y_train,
        batch_size=500,
        nb_epoch=epochs,
        validation_split=0.05)

    lstm.save_model_and_weights(model, '/projects/super-trader-ai/data/training/models/', file_name)

    predictions = []
    for index in range(10):
        prediction = lstm.predict_live(model, X_test[index], seq_len, prediction_len)
        denormalised_prediction = data_manager.denormalise_windows(raw_data, prediction)
        predictions.append(denormalised_prediction)

        # print(X_test[index])
        # print(np.reshape(prediction, (50,1)))

        denormalised_test = data_manager.denormalise_windows(raw_data, X_test[index + 1])
        mse = mean_squared_error(denormalised_test, np.reshape(denormalised_prediction, (prediction_len,1)))
        rmse = sqrt(mse)
        print('Test MSE: %.8f' % mse)
        print('Test RMSE: %.8f' % rmse)
        plot_results_multiple(denormalised_prediction, denormalised_test, prediction_len, file_name + str(index))



    # predictions = lstm.predict_sequences_multiple(model, X_test, seq_len, prediction_len)
    #
    # denormalised_predictions = data_manager.denormalise_windows(raw_data, predictions)
    # denormalised_test = data_manager.denormalise_windows(raw_data, X_test)

    # print(predictions)
    # print("Expected")
    # print(X_test[0, 1:])
    # rmse = sqrt(mean_squared_error(X_test[:seq_len], predictions))
    # print('Test RMSE: %.3f' % rmse)

    print('Training duration (s) : ', time.time() - global_start_time)
    # plot_results_multiple(predicted, real, prediction_len, file_name)
